﻿using System;
using InMemMenu.Core;
using InMemMenu.Core.Interfaces;

namespace InMemMenu.Demo.Models
{
    public class UserContextModel : IContext
    {
        public string ContextKey { get; set; }
        public Menu CurrentMenu { get; set; }
        public string CurrentCommand { get; set; }

        public UserContextModel()
        {
            CurrentMenu = MenuFactory.Instance.GetMenu("Main Menu");
            CurrentCommand = String.Empty;
        }

        public void DisplayPreviousMenu()
        {
            MenuFactory.Instance.SetActiveMenu(CurrentMenu.ParentTitle);
            MenuFactory.Instance.DisplayMenu();
        }
    }
}