﻿using System;
using System.Collections.Generic;
using InMemMenu.Core;
using InMemMenu.Core.Interfaces;

namespace InMemMenu.Demo
{
    public class Context : IContext
    {
        private Menu _currentMenu = MenuFactory.Instance.GetMenu("Main Menu");

        public Action<string> GetStateForCommandName(string commandName)
        {
            if (commandName == "add")
            {
                return parameter => DbFacade.StringList.GetOrAdd(parameter, user => new List<string>()).Add(parameter);
            }
            return emptyMethod => { };
        }

        public Menu CurrentMenu
        {
            get { return _currentMenu; }
            set { _currentMenu = value; }
        }
    }
}