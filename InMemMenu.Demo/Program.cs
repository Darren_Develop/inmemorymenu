﻿using System;
using InMemMenu.Core;
using InMemMenu.Core.Context;
using InMemMenu.Demo.Models;

namespace InMemMenu.Demo
{
    public class Program
    {
        public static HoldContext<UserContextModel> UserContext = new HoldContext<UserContextModel>();
        private static string _currentUser;

        public static void Main(string[] args)
        {
            DemoMenu.Create();
            _currentUser = "one";

            Run();
        }

        private static void Run()
        {

            while (true)
            {
                var commandName = Console.ReadLine();

                if (string.IsNullOrEmpty(commandName))
                {
                    continue;
                }

                if (commandName.IndexOf('!') >= 0)
                {
                    _currentUser = commandName.Replace("!user", string.Empty).Trim();
                    continue;
                }

                if (commandName.Equals("/exit"))
                {
                    break;
                }

                ExecuteReferenceCommandFor(_currentUser, commandName);
            }
        }

        private static void ExecuteReferenceCommandFor(string referenceName, string potentialCommand)
        {
            var reference = (UserContextModel)UserContext.GetContextFor(referenceName);
            var currentMenu = reference.CurrentMenu;

            var menuFactory = MenuFactory.Instance;

            menuFactory.SetActiveMenu(currentMenu);

            bool isCommand = currentMenu.ContainsSubCommand(potentialCommand);

            if (isCommand)
            {
                Console.Clear();
                var successfulExecute = menuFactory.ExecuteCommandFor(potentialCommand, currentMenu);

                if (successfulExecute)
                {
                    reference.CurrentCommand = potentialCommand;
                }
            }
            else
            {
                var parameter = potentialCommand;
                menuFactory.ExecuteCommandWithParameterFor(reference.CurrentCommand, parameter, reference, currentMenu);
            }

            reference.CurrentMenu = menuFactory.ActiveMenu;
        }
    }
}
