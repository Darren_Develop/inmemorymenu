﻿using InMemMenu.Core;
using InMemMenu.Core.Render;
using InMemMenu.Demo.ExecuteCommands;
using InMemMenu.Demo.ExecuteCommands.Database;

namespace InMemMenu.Demo
{
    public static class DemoMenu
    {
        private static MenuFactory _menuFactory;

        public static void Create()
        {
            _menuFactory = MenuFactory.Instance;
            _menuFactory.SetRenderer(RenderFor.Console);

            var mainMenu = CreateMainMenu();
            CreateOptionsMenu();
            CreateHelpMenu();
            CreateProfileMenu();

            _menuFactory.SetActiveMenu(mainMenu);
            _menuFactory.DisplayMenu();
        }

        private static void CreateHelpMenu()
        {
            var menu = _menuFactory.CreateNewMenu("Help Menu", "Main Menu");
        }

        private static void CreateOptionsMenu()
        {
            var menu = _menuFactory.CreateNewMenu("Options Menu", "Main Menu");

            menu.AddMenuItem(new MenuItem("change", "Change User", new ChangeReferenceCommand()));
            menu.AddMenuItem(new MenuItem("add", "Add to Static Menu", new AddToDatabaseCommand()));
            menu.AddMenuItem(new MenuItem("add", new AddToDatabase()));
            menu.AddMenuItem(new MenuItem("list", "List contents of Static Menu", new ListFromDatabaseCommand()));
            menu.AddMenuItem(new MenuItem("profile", "Go to Profile Options", _menuFactory.ShowMenuCommand("Profile Menu")));
        }

        private static void CreateProfileMenu()
        {
            var menu = _menuFactory.CreateNewMenu("Profile Menu", "Options Menu");

            menu.AddMenuItem(new MenuItem("email", "View Email", new NavigateCommand()));
        }

        private static Menu CreateMainMenu()
        {
            var menu = _menuFactory.CreateNewMenu("Main Menu");

            menu.AddMenuItem(new MenuItem(2, "website", "Website", new NavigateCommand()));
            menu.AddMenuItem(new MenuItem(1, "options", "Options", _menuFactory.ShowMenuCommand("Options Menu")));
            menu.AddMenuItem(new MenuItem(0, "help", "Help", _menuFactory.ShowMenuCommand("Help Menu")));

            return menu;
        }
    }
}
