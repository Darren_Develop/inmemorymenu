﻿using InMemMenu.Core.Interfaces;

namespace InMemMenu.Demo.Interfaces
{
    public interface IAddToDatabase : IAction
    {
        void Execute(string item);
    }
}
