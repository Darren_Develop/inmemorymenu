﻿using System;
using InMemMenu.Core.Interfaces;

namespace InMemMenu.Demo.ExecuteCommands.Database
{
    public class ListFromDatabaseCommand : IMenuCommand
    {
        public void Execute()
        {
            Console.Clear();
            Console.WriteLine("----- ITEMS -----");
            foreach (var item in DbMimic.StringList)
            {
                Console.WriteLine("// " + item.Key);
                foreach (var value in item.Value)
                {
                    Console.WriteLine(value);
                }

                Console.WriteLine();
                Console.WriteLine();
            }
            Console.WriteLine("-----------------");
        }
    }
}
