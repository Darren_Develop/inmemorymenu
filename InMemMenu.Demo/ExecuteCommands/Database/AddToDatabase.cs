﻿using System;
using System.Collections.Generic;
using InMemMenu.Core.Interfaces;
using InMemMenu.Demo.Models;

namespace InMemMenu.Demo.ExecuteCommands.Database
{
    public class AddToDatabase : IMenuCommandWithParameter
    {
        private IContext _context;

        public AddToDatabase()
        {
            _context = new UserContextModel();
        }

        public string Display()
        {
            throw new NotImplementedException();
        }

        public IMenuCommandWithParameter With(IContext context)
        {
            _context = context;
            return this;
        }

        public void Execute(object parameter)
        {
            var item = (string) parameter;

            if (item.Equals("/done", StringComparison.Ordinal))
            {
                ((UserContextModel) _context).DisplayPreviousMenu();
                return;
            }

            DbMimic.StringList.GetOrAdd(_context.ContextKey, list => new List<string>()).Add(item);

            Console.Clear();
            Console.WriteLine("Added your option to the list. Add some more or type /done to finish.");
        }
    }
}
