﻿using System;
using InMemMenu.Core.Interfaces;

namespace InMemMenu.Demo.ExecuteCommands.Database
{
    public class AddToDatabaseCommand : IMenuCommand
    {
        public void Execute()
        {
            Console.WriteLine("Please type in an item you would like to add to the list.");
        }
    }
}
