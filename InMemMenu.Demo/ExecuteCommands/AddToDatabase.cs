﻿using InMemMenu.Core.Interfaces;

namespace InMemMenu.Demo.ExecuteCommands
{
    public class AddToDatabase : IAction
    {
        public string Parameter { get; set; }
    }
}
