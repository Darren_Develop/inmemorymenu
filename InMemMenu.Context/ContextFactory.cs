﻿using System;
using System.Collections.Generic;

namespace InMemMenu.Context
{
    /// <summary>
    /// A Lazily Initiated Singleton class used to access Context Functionality throughout the application.
    /// Use ContextFactory.Instance gain access
    /// </summary>
    public class ContextFactory<T> where T : class, new()
    {
        ContextFactory() { } 

        /// <summary>
        /// Lazy Initiation of the ContextFactory.
        /// Is thread safe
        /// </summary>
        private static readonly Lazy<T> Lazy =
            new Lazy<T>(() => new T());

        /// <summary>
        /// Gets current Instance of ContextFactory
        /// </summary>
        public static T Instance
        {
            get
            {
                return Lazy.Value;
            }
        }

        public List<T> Current { get; set; }
    }
}
