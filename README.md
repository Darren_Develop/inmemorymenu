# README

In Memory Menu is a way to lazily load a thread-safe Menu into memory. It aims to be a fast and light weight menu management tool with the availability to add custom menu commands. The instance of a menu can be safely accessed through out an application without resolving or passing through.

## Code Examples


#### Accessing the MecuFactory Instance

```
#!c#
private static MenuFactory _menuFactory;

void Init(){
    var instance = MenuFactory.Instance;
}
```


#### Creating a Menu
######If the Menu Name is the same as the Display, you can omit the Menu Display Title completley
```
#!c#
void CreateMainMenu() {
    // "Menu Name", "Menu Display Title"
    var menu = _menuFactory.CreateNewMenu("Main Menu", "Main Menu");
}
```

#### Adding Menu Items
```
#!c#
void CreateMainMenu() {
    // "Menu Name", "Menu Display Title"
    var menu = _menuFactory.CreateNewMenu("Main Menu", "Main Menu");
    
    // "Command Name", "Command Display Title", Command()
    menu.AddMenuItem(new MenuItem("options", "Options", new MenuCommand()));
}
```
######You can also order the menu by setting an Index
```
#!c#
void CreateMainMenu() {
    // "Menu Name", "Menu Display Title"
    var menu = _menuFactory.CreateNewMenu("Main Menu", "Main Menu");

    // "Command Name", "Command Display Title", Command()
    menu.AddMenuItem(new MenuItem(2, "website", "Website", new MenuCommand()));
    menu.AddMenuItem(new MenuItem(1, "options", "Options", new MenuCommand()));
    
    // Will be displayed before the Options menu item
    menu.AddMenuItem(new MenuItem(0, "help", "Help", new MenuCommand()));
}
```

#### Navigating to a Menu
```
#!c#
void CreateMainMenu() {
    // "Menu Name", "Menu Display Title"
    var menu = _menuFactory.CreateNewMenu("Main Menu", "Main Menu");
    var optionsMenu = CreateOptionsMenu();
    
    // Pass the reference to the optionsMenu inside the Command()
    menu.AddMenuItem(new MenuItem(1, "options", "Options", optionsMenu));
}

Menu CreateOptionsMenu() {
    var optionsMenu  = _menuFactory.CreateNewMenu("Options Menu", "Options Menu");
    
    optionsMenu.AddMenuItem(new MenuItem("settings", "Settings", new MenuCommand()));
    
    return optionsMenu;
}
```

#### Display a Menu
######Call the `DisplayMenu()` command to get a string representation of the Menu
```
#!c#
void DisplayMenu() {
    var menuDisplayString = _menuFactory.DisplayMenu();
    
    console.WriteLine(menuDisplayString);
    // *Main Menu*
    // /help - *Display Text*
    // /options - *Display Text*
    // /website - *Display Text*
}
```