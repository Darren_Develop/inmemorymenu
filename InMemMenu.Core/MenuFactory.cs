﻿using System;
using System.Collections.Generic;
using System.Linq;
using InMemMenu.Core.Interfaces;
using InMemMenu.Core.Render;

namespace InMemMenu.Core
{
    /// <summary>
    /// A Lazily Initiated Singleton class used to access Menu Functionality throughout the application.
    /// Use MenuFactory.Instance gain access
    /// </summary>
    public class MenuFactory
    {
        /// <summary>
        /// Lazy Initiation of the MenuFactory.
        /// Is thread safe
        /// </summary>
        private static readonly Lazy<MenuFactory> Lazy =
            new Lazy<MenuFactory>(() => new MenuFactory
            {
                _menus = new List<Menu>(),
                _renderer = new RenderConsole()
            });

        /// <summary>
        /// Gets current Instance of MenuFactory
        /// </summary>
        public static MenuFactory Instance
        {
            get
            {
                return Lazy.Value;
            }
        }

        /// <summary>
        /// Gets the Currently Active Top Menu
        /// </summary>
        public Menu ActiveMenu
        {
            get
            {
                return _activeMenu;
            }
        }

        public IEnumerable<string> AvailableCommands
        {
            get { return _activeMenu.MenuItems.Select(x => x.QueryName); }
        }

        public MenuItem GlobalCommand
        {
            get
            {
                return _globalCommand;
            }
            set { _globalCommand = new MenuItem(RemoveSpecialCharactersFromString(value.QueryName), value.Title, value.MenuQuery); }
        }

        private List<Menu> _menus;
        private Menu _activeMenu;
        private IRenderer _renderer;
        private MenuItem _globalCommand;

        /// <summary>
        /// Create a new Top Level Menu
        /// </summary>
        /// <param name="menuTitle">
        /// Title of the Menu. Used as reference when searching for a Menu.
        /// </param>
        /// <param name="parentTitle">
        /// Title of the parent Menu if any
        /// </param>
        /// <returns>
        /// Menu as a new Menu
        /// </returns>
        public Menu CreateNewMenu(string menuTitle, string parentTitle = null, string subText = null)
        {
            Menu menu = new Menu
            {
                Title = menuTitle,
                ParentTitle = parentTitle,
                SubText = subText
            };

            if (!string.IsNullOrEmpty(parentTitle))
            {
                menu.AddMenuItem(new MenuItem(100, "back", "Back to " + menu.ParentTitle, ShowMenuCommand(parentTitle)));
            }

            _menus.Add(menu);

            return menu;
        }

        /// <summary>
        /// Finds a Command within the current active Menu and attempts to Execute it
        /// </summary>
        /// <param name="commandName">
        /// Name of the Command to Execute
        /// </param>
        /// <param name="context">
        /// Optional Context
        /// </param>
        public bool ExecuteQuery(string commandName, IContext context)
        {
            var menuItem = GetMenuItem(commandName);

            if (menuItem != null)
            {
                menuItem.MenuQuery.With(context).Execute();
                return true;
            }

            Console.WriteLine("No command with that name");
            return false;
        }

        /// <summary>
        /// Finds a Command within the current active Menu and attempts to Execute it
        /// </summary>
        /// <param name="commandName">
        /// Name of the Command to Execute
        /// </param>
        /// <param name="menu">
        /// Menu to check against
        /// </param>
        /// <param name="context">
        /// Optional Context
        /// </param>
        public string ExecuteQueryFor(string commandName, Menu menu, IContext context)
        {
            var menuItem = GetMenuItemFor(commandName, menu);

            if (menuItem != null)
            {
                menuItem.MenuQuery.With(context).Execute();
                return menuItem.MenuQuery.Display();
            }

            return "Error: No command with that name";
        }

        /// <summary>
        /// Finds a Command within the current active Menu and attempts to Execute it with Parameters
        /// </summary>
        /// <param name="commandName">
        /// Name of the Command to Execute
        /// </param>
        /// <param name="parameter">
        /// Paramaters to add
        /// </param>
        /// <param name="context">
        /// Optional Context
        /// </param>
        public bool ExecuteCommand(string commandName, object parameter, IContext context)
        {
            var menuItem = GetMenuItemWithParameter(commandName);

            if (menuItem != null)
            {
                menuItem.MenuCommand.With(context).Execute(parameter);
                return true;
            }

            Console.WriteLine("No command with that name");
            return false;
        }

        /// <summary>
        /// Finds a Command within the current active Menu and attempts to Execute it with Parameters
        /// </summary>
        /// <param name="commandName">
        /// Name of the Command to Execute
        /// </param>
        /// <param name="parameter">
        /// Paramaters to add
        /// </param>
        /// <param name="context">
        /// Optional Context
        /// </param>
        /// <param name="menu">
        /// Menu to check against
        /// </param>
        public string ExecuteCommandFor(string commandName, object parameter, IContext context, Menu menu)
        {
            var menuItem = GetMenuItemWithParameterFor(commandName, menu);

            if (menuItem != null)
            {
                menuItem.MenuCommand.With(context).Execute(parameter);
                return menuItem.MenuCommand.Display();
            }
            
            return "Error: No command with that name";
        }

        /// <summary>
        /// Changes the current Active Menu
        /// Forces all subsequent commands to change context to the new Active Menu
        /// </summary>
        /// <param name="menu">
        /// Menu to set
        /// </param>
        public void SetActiveMenu(Menu menu)
        {
            _activeMenu = menu;
        }

        /// <summary>
        /// Changes the current Active Menu
        /// Forces all subsequent commands to change context to the new Active Menu
        /// </summary>
        /// <param name="menuTitle">
        /// Title of Menu to set
        /// </param>
        public void SetActiveMenu(string menuTitle)
        {
            var menu = GetMenu(menuTitle);
            SetActiveMenu(menu);
        }

        /// <summary>
        /// Displays the current Active Menu commands in the Console
        /// </summary>
        public string DisplayMenu()
        {
            return _renderer.Render();
        }

        /// <summary>
        /// Gets a Menu from the list of Top Level Menus
        /// </summary>
        /// <param name="menuName">
        /// Name of the Menu to get
        /// </param>
        /// <returns>
        /// Menu or Null
        /// </returns>
        public virtual Menu GetMenu(string menuName)
        {
            return _menus.FirstOrDefault(x => x.Title == menuName);
        }

        /// <summary>
        /// COMMAND to Show a new Menu
        /// </summary>
        /// <param name="menuName">
        /// Name of Menu to show
        /// </param>
        /// <returns>
        /// new NavigateToMenu command
        /// </returns>
        public virtual NavigateToMenu ShowMenuCommand(string menuName)
        {
            return new NavigateToMenu(menuName);
        }

        /// <summary>
        /// Sets the renderer for the Display method
        /// </summary>
        /// <param name="renderer">
        /// IRenderer types
        /// </param>
        public void SetRenderer(IRenderer renderer)
        {
            _renderer = renderer;
        }

        /// <summary>
        /// Checks of the current requested command is in the list of currently available commands
        /// </summary>
        /// <param name="queryName">
        /// Name of potential command
        /// </param>
        /// <returns>
        /// true / false
        /// </returns>
        public bool IsInAvailableCommands(string queryName)
        {
            queryName = RemoveSpecialCharactersFromString(queryName);
            string finishCommandName = String.Empty;
            if (GlobalCommand != null)
            {
                finishCommandName = GlobalCommand.QueryName;
            }
            return AvailableCommands.Contains(queryName) || finishCommandName == queryName;
        }

        private MenuItem GetMenuItem(string queryName)
        {
            queryName = RemoveSpecialCharactersFromString(queryName);

            var menuItem =
                _activeMenu.MenuItems.Where(x => x.MenuQuery != null).FirstOrDefault(x => x.QueryName == queryName);
            return menuItem;
        }

        private MenuItem GetMenuItemFor(string queryName, Menu menu)
        {
            queryName = RemoveSpecialCharactersFromString(queryName);

            var menuItem = menu.MenuItems.Where(x => x.MenuQuery != null).FirstOrDefault(x => x.QueryName == queryName);
            return menuItem;
        }

        private MenuItem GetMenuItemWithParameter(string queryName)
        {
            queryName = RemoveSpecialCharactersFromString(queryName);

            var menuItem =
                _activeMenu.MenuItems.Where(x => x.MenuCommand != null).FirstOrDefault(x => x.QueryName == queryName);
            return menuItem;
        }

        private MenuItem GetMenuItemWithParameterFor(string queryName, Menu menu)
        {
            queryName = RemoveSpecialCharactersFromString(queryName);

            var menuItem =
                menu.MenuItems.Where(x => x.MenuCommand != null).FirstOrDefault(x => x.QueryName == queryName);
            return menuItem;
        }

        public static string RemoveSpecialCharactersFromString(string text)
        {
            if (text.IndexOf("/", StringComparison.Ordinal) > -1)
            {
                text = text.TrimStart('/');
            }
            return text;
        }
    }
}