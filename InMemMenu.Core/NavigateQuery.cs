﻿using InMemMenu.Core.Interfaces;

namespace InMemMenu.Core
{
    /// <summary>
    /// Default Navigation Command with NO functionality
    /// </summary>
    public class NavigateQuery : IMenuQuery
    {

        public void Execute()
        {
        }

        public string Display()
        {
            return "";
        }

        public IMenuQuery With(IContext context)
        {
            return this;
        }
    }
}