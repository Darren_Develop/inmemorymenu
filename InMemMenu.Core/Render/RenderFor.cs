﻿using InMemMenu.Core.Interfaces;

namespace InMemMenu.Core.Render
{
    public class RenderFor
    {

        public static IRenderer Console
        {
            get
            {
                return new RenderConsole();
            }
        }

        public static IRenderer Telegram
        {
            get
            {
                return new RenderTelegram();
            }
        }
    }
}
