﻿using System.Linq;
using System.Text;
using InMemMenu.Core.Interfaces;

namespace InMemMenu.Core.Render
{
    public sealed class RenderConsole : IRenderer
    {
        public string Render()
        {
            var menuFactory = MenuFactory.Instance;
            var activeMenu = menuFactory.ActiveMenu;

            StringBuilder menu = new StringBuilder();

            menu.AppendLine(activeMenu.Title);
            menu.AppendLine("-------------------");
            var nonHiddenItems = activeMenu.MenuItems.Where(x => !x.Hidden);

            var orderedMenuItems = nonHiddenItems.OrderBy(x => x.Index);
            foreach (var menuItem in orderedMenuItems)
            {
                menu.AppendLine("- /" + menuItem.QueryName + " : " + menuItem.Title);
            }
            menu.AppendLine("-------------------");

            return menu.ToString();
        }
    }
}
