﻿using System.Linq;
using System.Text;
using InMemMenu.Core.Interfaces;

namespace InMemMenu.Core.Render
{
    public class RenderTelegram : IRenderer
    {
        public string Render()
        {
            var menuFactory = MenuFactory.Instance;
            var activeMenu = menuFactory.ActiveMenu;

            StringBuilder menu = new StringBuilder();

            menu.AppendLine("*" + activeMenu.Title + "*");
            if (!string.IsNullOrEmpty(activeMenu.SubText))
            {
                menu.AppendLine("_" + activeMenu.SubText + "_");
            }
            menu.AppendLine();
            var orderedMenuItems = activeMenu.MenuItems.Where(x => !x.Hidden).OrderBy(x => x.Index);
            foreach (var menuItem in orderedMenuItems)
            {
                menu.AppendLine("/" + menuItem.QueryName + " - " + menuItem.Title);
            }

            return menu.ToString();
        }
    }
}
