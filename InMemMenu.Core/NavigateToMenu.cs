﻿using InMemMenu.Core.Interfaces;

namespace InMemMenu.Core
{
    /// <summary>
    /// Command to Easilly navigate to a Top Level Menu with no order of Initialization
    /// </summary>
    public sealed class NavigateToMenu : IMenuQuery
    {
        private readonly string _menuName;

        /// <summary>
        /// Sets the Paramaters to Navigate to a Top Level Menu
        /// </summary>
        /// <param name="menuName">
        /// Name of Top Level Menu to Navigate to</param>
        public NavigateToMenu(string menuName)
        {
            _menuName = menuName;
        }

        /// <summary>
        /// Command to get the requested Top Level Menu and navigate
        /// </summary>
        public void Execute()
        {
            var menuFactory = MenuFactory.Instance;

            var menu = menuFactory.GetMenu(_menuName);
            menuFactory.SetActiveMenu(menu);
            menuFactory.DisplayMenu();
        }

        public string Display()
        {
            return MenuFactory.Instance.DisplayMenu();
        }

        public IMenuQuery With(IContext context)
        {
            return this;
        }
    }
}
