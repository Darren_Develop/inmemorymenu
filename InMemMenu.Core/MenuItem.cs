﻿using System;
using InMemMenu.Core.Interfaces;

namespace InMemMenu.Core
{
    /// <summary>
    /// Used to create a new Menu Item for a Top Level Menu
    /// </summary>
    public sealed class MenuItem
    {
        /// <summary>
        /// Title of the Menu Item
        /// Used as a reference
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Interfaced command to eventually execute
        /// </summary>
        public IMenuQuery MenuQuery { get; set; }
        /// <summary>
        /// Potential parameters to execute
        /// </summary>
        public IMenuCommand MenuCommand { get; set; }
        /// <summary>
        /// Short Command Name to call the Menu item
        /// </summary>
        public string QueryName { get; set; }
        /// <summary>
        /// Optional Index of the Menu Item (Used for listing order)
        /// </summary>
        public int Index { get; set; }

        public bool ExpectsParameter { get; set; }

        public bool Hidden { get; set; }

        /// <summary>
        /// Menu Item Constructor
        /// </summary>
        /// <param name="index">Display Index of the Menu Item</param>
        /// <param name="commandName">Command Name of the Menu Item (Without Spaces)</param>
        /// <param name="title">Title of the Command Item (Used for Display and Reference)</param>
        /// <param name="menuQuery">Command of the Menu Item</param>
        /// <param name="hidden">Hidden from the user</param>
        /// <param name="expectsParameter">Item expects parameters</param>
        public MenuItem(int index, string commandName, string title, IMenuQuery menuQuery, bool hidden = false, bool expectsParameter = false)
        {
            // Make sure the Command Name has no Spaces
            if (commandName.IndexOf(' ') > 0)
            {
                throw new Exception("Command Name must not have Spaces");
            }

            // Make sure there is an attached Command
            if (menuQuery == null)
            {
                throw new Exception("Menu Command can not be null");
            }

            Title = title;
            Index = index;
            QueryName = commandName;
            MenuQuery = menuQuery;
        }

        /// <summary>
        /// Menu Item Constructor
        /// </summary>
        /// <param name="commandName">Command Name of the Menu Item (Without Spaces)</param>
        /// <param name="menuCommand">Command of the Menu Item</param>
        /// <param name="hidden">Hidden from the user</param>
        /// <param name="expectsParameter">Item expects parameters</param>
        public MenuItem(string commandName, IMenuCommand menuCommand, bool hidden = true, bool expectsParameter = true)
        {
            // Make sure the Command Name has no Spaces
            if (commandName.IndexOf(' ') > 0)
            {
                throw new Exception("Command Name must not have Spaces");
            }

            // Make sure there is an attached Command
            if (menuCommand == null)
            {
                throw new Exception("Menu Command can not be null");
            }

            Title = string.Empty;
            Index = 0;
            QueryName = commandName;
            MenuCommand = menuCommand;
            Hidden = hidden;
            ExpectsParameter = expectsParameter;
        }

        /// <summary>
        /// Menu Item Constructor
        /// </summary>
        /// <param name="commandName">Command Name of the Menu Item (Without Spaces)</param>
        /// <param name="title">Title of the Command Item (Used for Display and Reference)</param>
        /// <param name="menuQuery">Command of the Menu Item</param>
        public MenuItem(string commandName, string title, IMenuQuery menuQuery)
        {
            Title = title;
            Index = 0;
            QueryName = commandName;
            MenuQuery = menuQuery;
        }


        /// <summary>
        /// Menu Item Constructor with Parameters
        /// </summary>
        /// <param name="commandName">Command Name of the Menu Item (Without Spaces)</param>
        /// <param name="title">Title of the Command Item (Used for Display and Reference)</param>
        /// <param name="menuCommand">Parameters of the Menu Item</param>
        public MenuItem(string commandName, string title, IMenuCommand menuCommand)
        {
            Title = title;
            Index = 0;
            QueryName = commandName;
            MenuCommand = menuCommand;
        }
    }
}