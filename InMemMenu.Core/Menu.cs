﻿using System;
using System.Collections.Generic;
using System.Linq;
using InMemMenu.Core.Interfaces;

namespace InMemMenu.Core
{
    /// <summary>
    /// Top level menu, meant to house Menu Items
    /// Has an Execute command to easilly be called from other Menu Items
    /// </summary>
    public sealed class Menu : IMenuQuery
    {
        private readonly List<MenuItem> _menuItems = new List<MenuItem>();
        /// <summary>
        /// Title of the Menu
        /// Used as reference when searching for a Top level Menu
        /// </summary>
        public string Title { get; set; }
        public string SubText { get; set; }
        /// <summary>
        /// A list of Menu Items
        /// </summary>
        public List<MenuItem> MenuItems { get { return _menuItems; } }

        public string ParentTitle { get; set; }

        /// <summary>
        /// Add a new Item to the Menu
        /// </summary>
        /// <param name="menuItem"></param>
        public void AddMenuItem(MenuItem menuItem)
        {
            _menuItems.Add(menuItem);
        }
        /// <summary>
        /// Changes the active Menu to this and displays
        /// </summary>
        public void Execute()
        {
            MenuFactory menuFactory = MenuFactory.Instance;
            menuFactory.SetActiveMenu(this);
            //menuFactory.DisplayMenu();
        }

        /// <summary>
        /// Checks if any of the menu items has a command
        /// </summary>
        /// <param name="queryName"></param>
        /// <returns></returns>
        public bool ContainsSubQuery(string queryName)
        {
            queryName = MenuFactory.RemoveSpecialCharactersFromString(queryName);

            return
                MenuItems.Any(
                    x =>
                        x.QueryName.Equals(queryName,
                            StringComparison.OrdinalIgnoreCase));
        }

        public string Display()
        {
            return MenuFactory.Instance.DisplayMenu();
        }

        public IMenuQuery With(IContext context)
        {
            return this;
        }
    }
}