﻿namespace InMemMenu.Core.Interfaces
{
    public interface IHoldEntityContext
    {
        IContext GetContextFor(string reference);
    }
}