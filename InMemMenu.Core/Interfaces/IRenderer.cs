﻿namespace InMemMenu.Core.Interfaces
{
    public interface IRenderer
    {
        string Render();
    }
}
