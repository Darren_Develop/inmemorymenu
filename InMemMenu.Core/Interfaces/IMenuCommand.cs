﻿using System;

namespace InMemMenu.Core.Interfaces
{
    public interface IMenuCommand
    {
        void Execute(object parameter);
        string Display();
        IMenuCommand With(IContext context);
    }
}
