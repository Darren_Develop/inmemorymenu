﻿namespace InMemMenu.Core.Interfaces
{
    public interface IContext
    {
        string ContextKey { get; set; }
    }
}