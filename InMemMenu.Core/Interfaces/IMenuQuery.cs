﻿namespace InMemMenu.Core.Interfaces
{
    /// <summary>
    /// All commands needed to be executed by a menu item should inherit from this
    /// </summary>
    public interface IMenuQuery
    {
        void Execute();
        string Display();
        IMenuQuery With(IContext context);
    }
}