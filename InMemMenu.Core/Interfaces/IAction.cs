﻿namespace InMemMenu.Core.Interfaces
{
    public interface IAction<in T>
    {
        void Execute(T item);
    }
}