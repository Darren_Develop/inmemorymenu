﻿using System.Collections.Concurrent;
using InMemMenu.Core.Interfaces;

namespace InMemMenu.Core.Context
{
    public class HoldContext<T> where T : class, IContext, new()
    {
        readonly ConcurrentDictionary<string, T> _contexts = new ConcurrentDictionary<string, T>();

        public IContext GetContextFor(string reference)
        {
            var contextFor = _contexts.GetOrAdd(reference, AddNewContextForEntity);
            contextFor.ContextKey = reference;

            return contextFor;
        }

        private T AddNewContextForEntity(string arg)
        {
            return new T();
        }
    }
}